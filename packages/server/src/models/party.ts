import mongoose from 'mongoose'
import generate from 'nanoid/generate'

export default mongoose.model('Party', new mongoose.Schema({
  code: {
    type: String,
    unique: true,
    default: () => generate('ABCDEFGHIJKLMNOPQRSTUVWXYZ', 1)
  },
  title: String,
}))
