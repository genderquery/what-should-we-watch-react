import React from 'react'
import { useParams } from 'react-router-dom'

export type Props = {
}

const ViewingParty: React.FunctionComponent<Props> = () => {
  const { partyCode } = useParams()
  return (
    <div>
      <h2>Welcome to party {partyCode}</h2>
    </div>
  )
}

export default ViewingParty
