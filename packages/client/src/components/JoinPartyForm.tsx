import React, { useState, ChangeEventHandler, FormEventHandler } from 'react'
import { useHistory } from 'react-router-dom'

const JoinPartyForm: React.FunctionComponent = () => {
  const history = useHistory()
  const [partyCode, setPartyCode] = useState('')
  const onChangePartyCode: ChangeEventHandler<HTMLInputElement> = e => {
    setPartyCode(e.target.value.toUpperCase())
  }
  const onSubmitForm: FormEventHandler<HTMLFormElement> = e => {
    e.preventDefault()
    history.push(`/${partyCode}`)
  }
  return (
    <form onSubmit={onSubmitForm}>
      <label>Party Code:</label>
      <input type="text" value={partyCode} onChange={onChangePartyCode} />
      <input type="submit" value="Go" />
    </form>
  )
}

export default JoinPartyForm
