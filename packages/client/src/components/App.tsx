import React, { useState, useEffect } from 'react'
import { BrowserRouter as Router, Switch, Route, Link, useHistory } from 'react-router-dom'

import JoinPartyForm, { SubmitEventHandler } from './JoinPartyForm'
import ViewingParty from './ViewingParty'

const App: React.FunctionComponent = () => {
  return (
    <Router>
      <h1>What Should We Watch?</h1>
      <Switch>
        <Route exact path="/">
          <JoinPartyForm />
        </Route>
        <Route path="/:partyCode">
          <ViewingParty />
        </Route>
      </Switch>
    </Router>
  )
}

export default App
