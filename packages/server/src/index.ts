import http from 'http'
import { AddressInfo, ListenOptions } from 'net'

import './db'

import app from './app'

const server = http.createServer(app)

server.on('error', (err) => {
  console.error(err)
  process.exit(1)
})

server.listen({
  port: parseInt(process.env.PORT || '', 10) || 3000,
  host: process.env.HOST || '0.0.0.0',
} as ListenOptions, () => {
  const { address, port } = server.address() as AddressInfo
  console.log(`Listening on http://${address}:${port}/`)
})
