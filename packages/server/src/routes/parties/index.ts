import express from 'express'
import { Party } from '../../models'

import { Document } from 'mongoose'

const router = express.Router()

router.get('/', (req, res, next) => {
  Party
    .find()
    .lean()
    .exec((err, documents) => {
      if (err) {
        next(err)
      } else {
        res.json({
          results: documents
        })
      }
    })
})

router.post('/', (req, res, next) => {
  let promise = Promise.reject<Document>()
  for (let tries = 10; tries > 0; tries--) {
    promise = promise.catch(() => new Party({ title: req.body.title }).save())
  }
  promise
    .then(document => res.status(201).json(document))
    .catch(next)
})

router.get('/:code', (req, res, next) => {
  Party
    .findOne({ code: req.params.code })
    .lean(true)
    .exec()
    .then(document => {
      if (document) {
        res.json(document)
      } else {
        next()
      }
    })
    .catch(err => next(err))
})

export default router
