import mongoose from 'mongoose'

mongoose.connect(process.env.MONGODB_URI!, {
  useNewUrlParser: true
}, (err => {
  if (err) {
    console.error(err)
  } else {
    console.log(`Connected to ${process.env.MONGODB_URI}`)
  }
}))
