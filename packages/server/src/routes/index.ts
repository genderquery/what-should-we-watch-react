import express from 'express'

import parties from './parties'

const router = express.Router()

router.get('/', (req, res) => {
  res.json({
    version: '0.0.1'
  })
})

router.use('/parties', parties)

export default router
