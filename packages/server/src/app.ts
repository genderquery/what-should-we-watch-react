import cors from 'cors'
import express from 'express'
import path from 'path'
import morgan from 'morgan'

import routes from './routes'

const app = express()

app.use(cors())
app.use(morgan('dev'))
app.use(express.json())
app.use(express.static(path.join(__dirname, '../../client/dist')))

app.use('/api', routes)

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../../client/dist/index.html'))
})

export default app
